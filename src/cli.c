#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "../lib/conf.h"
#include "../lib/util.h"
#include "../lib/event.h"

/* CLI flags */
#define WIKI_ARG "-W"
#define EVENT_ARG "-E"
#define LIMIT_MIN "--LN"
#define LIMIT_MAX "--LX"
#define CATEGORY_ARG "--CAT"
#define TITLE_ARG "--TITLE"
#define HELP_FLAG "--help"

static void help();
static void warn(const char *);

static void parse_args(int, char **, struct config *, struct limit *);

int
main(int argc, char **argv)
{
	EventTree *et_root;
	struct limit lim;

	struct config conf = conf_from_env(argc, argv);
	lim = limit_new(&conf);

	parse_args(argc, argv, &conf, &lim);

	et_root = eventtree_from(&conf);

	if (lim.title_tar != NULL) eventtree_in_order(et_root, &conf, &lim, event_vdisp);
	else eventtree_in_order(et_root, &conf, &lim, event_disp);

	et_root = eventtree_free(et_root);
	free(lim.min);
	free(lim.max);
	return 0;
}

static void
help(void)
{
    printf(
    "Query the disorg base directory.\n"
    "    --COUNT : display the number of events\n"
    "         " WIKI_ARG " : display wiki files (instead of event files)\n"
    // "      -E : display event files (instead of wiki files, default\n"
    "  " TITLE_ARG " T : display only events/wikis matching the given title.\n"
    "  " CATEGORY_ARG " CAT : display only events/wikis matching the given category.\n"
    "   " LIMIT_MAX " MAX : display events max days in advance at most.\n"
    "   " LIMIT_MIN " MIN : display events min days prior to current time at most.\n"
    "     " HELP_FLAG " : display this help message.\n"
    );
    exit(0);
}

static void
warn(const char *s)
{
    printf("Warning: %s\n", s);
}

static void
parse_args(int argc, char **argv, struct config *conf, struct limit *lim)
{
	for (int i = 0; i < argc; ++i) {
	    if (strcmp(argv[i], HELP_FLAG) == 0) help();

	    else if (strcmp(argv[i], WIKI_ARG) == 0) conf->wiki = true;
	    else if (strcmp(argv[i], EVENT_ARG) == 0) conf->wiki = false;

	    else if (strcmp(argv[i], LIMIT_MAX) == 0) {
	        if (lim->max == NULL) continue;
	        if (++i < argc) lim->max->tm_mday = conf->now->tm_mday + atoi(argv[i]);
	        else warn("LX requires an argument.");
	    }

	    else if (strcmp(argv[i], LIMIT_MIN) == 0) {
	        if (lim->min == NULL) continue;
	        if (++i < argc) lim->min->tm_mday = conf->now->tm_mday - atoi(argv[i]);
	        else warn("LN requires an argument.");
	    }

	    else if (strcmp(argv[i], CATEGORY_ARG) == 0) {
            if (++i < argc) lim->categ_tar = argv[i];
            else warn("Category filter requires an argument.");
	    }

	    else if (strcmp(argv[i], TITLE_ARG) == 0) {
            if (++i < argc) lim->title_tar = argv[i];
            else warn("Title filter requires an argument.");
	    }
	}
}
