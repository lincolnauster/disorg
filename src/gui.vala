public class Gui : Gtk.Application {
    private Gtk.HeaderBar hb;

    private Gtk.Box vert_nav;
    private Gtk.Box horz_nav;

    private Gtk.Button up_button;
    private Gtk.Button down_button;
    private Gtk.Button left_button;
    private Gtk.Button right_button;

    private Gtk.ListBox events;
    private Gtk.ListBox wikis;

    private Gtk.Notebook main;

    public Gui() {
        Object(application_id: "org.disorg.gtk");
    }

    public override void activate() {
        var window = new Gtk.Window();
        window.set_title("disorg.gtk");
        window.set_default_size(600, 800);
        window.title = "disorg.gtk";

        this.setup_header(window);
        this.setup_events();
        this.setup_wikis();
        this.setup_main(window);

        this.add_window(window);

        window.show();
    }

    private void next_tab(Gtk.Button _) { this.main.next_page(); }
    private void prev_tab(Gtk.Button _) { this.main.prev_page(); }

    private inline void setup_events() {
        this.events = new Gtk.ListBox();
    }

    private inline void setup_wikis() {
        this.wikis = new Gtk.ListBox();
    }

    private inline void setup_main(Gtk.Window window) {
        this.main = new Gtk.Notebook();
        this.main.show_tabs = false;

        this.main.append_page(
            this.events,
            new Gtk.Label("Events")
        );
        this.main.append_page(
            this.wikis,
            new Gtk.Label("Wikis")
        );

        window.child = this.main;
    }

    private void setup_header(Gtk.Window window) {
        this.hb = new Gtk.HeaderBar();
        this.hb.show_title_buttons = true;

        this.vert_nav = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 4);
        this.horz_nav = new Gtk.Box(Gtk.Orientation.HORIZONTAL, 4);

        this.up_button = new Gtk.Button();
        this.down_button = new Gtk.Button();
        this.left_button = new Gtk.Button();
        this.right_button = new Gtk.Button();
        this.left_button.clicked.connect(this.prev_tab);
        this.right_button.clicked.connect(this.next_tab);

        this.up_button.set_child(
            new Gtk.Image.from_gicon(new GLib.ThemedIcon("go-up-symbolic"))
        );
        this.down_button.set_child(
            new Gtk.Image.from_gicon(new GLib.ThemedIcon("go-down-symbolic"))
        );

        this.left_button.set_child(
            new Gtk.Label("Events")
        );
        this.right_button.set_child(
            new Gtk.Label("Wikis")
        );

        hb.pack_start(this.horz_nav);
        hb.pack_end(this.vert_nav);

        this.vert_nav.append(this.up_button);
        this.vert_nav.append(this.down_button);
        this.horz_nav.append(this.left_button);
        this.horz_nav.append(this.right_button);

        window.set_titlebar(hb);

    }
}

int main(string[] args) {
    var app = new Gui();
    return app.run(args);
}
