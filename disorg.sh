#!/usr/bin/env bash

# main.c loads config from environment variables, so defaults are set and the
# config is loaded in (the redirection to /dev/null prevents an error message
# if the config file doesn't exist. DEBUG is not loaded by main.c; if enabled
# the script runs valgrind to check for memory leaks.

# disorg.sh options
BASE_DIRECTORY="$HOME/.disorg"
DEBUG="FALSE"
PATTERN='*.ev'

# COUNT can be specified to return the number of events, in total, located
# in the BASE_DIRECTORY. This does _not_ check if the events are valid,
# just the existance of the file.
if [[ "$1" == "COUNT" ]]; then
	echo "COUNT: $(find $BASE_DIRECTORY -type f -name "*.ev" | wc -l)"
	exit 0
fi

# The W option controls a) the files that are passed to the binary, and b) the
# order in which the files are sorted on output - wiki files have no associated
# time, and as such cannot be sorted by that attribute. Instead, they are sorted
# alphabetically.
if [[ "$@" == *"-W"* ]]; then
	PATTERN='*.wi'
fi


# disorg-main config file options
DATE_FORMAT="D-M-Y"
TIME_FORMAT="H:M"
FOUR_DIGIT_YEAR="TRUE"

TODAY_COLOR='91'
PHIGH_COLOR='31'
PMED_COLOR='35'
PLOW_COLOR='0'

DAYS_AFTER=10
DAYS_BEFORE=10

source ~/.config/disorg/disorg 2> /dev/null
export FOUR_DIGIT_YEAR
export DATE_FORMAT
export TIME_FORMAT
export TODAY_COLOR
export PHIGH_COLOR
export PMED_COLOR
export PLOW_COLOR

# disorg-main CLI args
TARGET=""
CATEGORY=""
WIKI="FALSE"
# check from pattern
if [[ "$PATTERN" == "*.wi" ]]; then
	WIKI="TRUE"
fi

# -T option specifies target/title
for ((i = 1 ; i < $# ; i++ )); do
	if [[ "${!i}" == "-T" ]]; then
		i=$(($i + 1))
		TARGET="${!i}"
	fi
done

export BASE_DIRECTORY
export CATEGORY
export DAYS_AFTER
export DAYS_BEFORE
export TARGET
export WIKI

# get any parameters from the environment or external tools
read -r _ COL_WIDTH <<< $(stty size)
export COL_WIDTH

FILES=$(find "$BASE_DIRECTORY" -type f -name "$PATTERN")
# Call the main binary: write all files matching args to stdin.
if [[ "$DEBUG" == "TRUE" ]]; then
	time 2> valgrind.log valgrind --leak-check=full \
	   $(dirname $0)/disorg-main "$@" <<< $FILES
else
	$(dirname $0)/disorg-main "$@" <<< "$FILES"
fi
