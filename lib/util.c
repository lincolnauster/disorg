#include <libgen.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "conf.h"
#include "util.h"
#include "event.h"

#define CHECK_REALLOC(buf, new_buf) \
if (new_buf == NULL) { \
    free(*buffer);     \
    *buffer = NULL;    \
    return;            \
} else *buffer = nb;

/* append given character to the buffer, resize if necessary */
void
buffer_append(char **buffer, const char c, size_t *capacity)
{
	if (*buffer == NULL) {
		*buffer = malloc(sizeof(char));
		*buffer[0] = '\0';
	}

	size_t text_len = strlen(*buffer);
	if (text_len + 2 > *capacity) {
		*capacity *= 2;
		char *nb = realloc(*buffer, *capacity * sizeof(char));
		CHECK_REALLOC(buffer, nb);
	}

	(*buffer)[text_len++] = c;
	(*buffer)[text_len] = '\0';
}

/* append given C-string to the buffer, resize if necessary */
void
buffer_append_str(char **buffer, const char *string, size_t *capacity)
{
	if (*buffer == NULL) {
		*buffer = strdup(string);
		*capacity = sizeof(string);
		return;
	}

	size_t text_len = strlen(*buffer) + strlen(string) + 1;
	if (text_len > *capacity) {
		*capacity = text_len;
		char *nb = realloc(*buffer, *capacity);
		CHECK_REALLOC(buffer, nb);
	}

	for (size_t i = 0; i < strlen(string); ++i)
	    buffer_append(buffer, string[i], capacity);
}

/* Procedurally acquire a color for a given string */
unsigned int
buftocol(const char *s)
{
	unsigned int c = 0;
	for (size_t i = 0; i < strlen(s); i++)
		c += s[i];
	/* limit to 16 colors to preserve terminal themeing */
	return 1 + c % 14;
}

/* find the parent dir given a / delimited filename - string returned
 * is dynamically allocated */
char *
parent_dir(const char *p)
{
	char path[strlen(p) + 1];
	strcpy(path, p);
	char *without_name = dirname(path);
	char *parent_dir   = basename(without_name);

	return strdup(parent_dir);
}

/* return an empty tm_struct (i.e., initialized to 0:0 0/0/1900) */
struct tm *
tm_empty(void)
{
	struct tm *t;
	t = malloc(sizeof(struct tm));
	t->tm_sec  = 0;
	t->tm_min  = 0;
	t->tm_hour = 0;
	t->tm_mday  = 1;
	t->tm_mon   = 0;
	t->tm_year  = 0;
	t->tm_yday  = 0;
	t->tm_isdst = 0;

	return t;
}

/* get a heap-allocated time (not date) string out of a config and struct tm */
char *
tm_tascii(const struct tm *t)
{
	char *time = malloc(7);
	sprintf(time, "%02d:%02d", t->tm_hour, t->tm_min);
	return time;
}

/* get a heap-allocated date (not time) string out of a config and struct tm */
char *
tm_dascii(const struct tm *t, const struct config *c)
{
	char *date;

	char mday[3], mon[3], year[5];
	int mday_pos, mon_pos, year_pos, yearlen;
	char delim;

	delim = first_nonalpha(c->date_format);
	date = malloc(11);

	date[2] = date[5] = delim;
	date[10] = '\0';

	sprintf(mday, "%02d", t->tm_mday);
	sprintf(mon,  "%02d", t->tm_mon + 1);
	if (c->four_digit_year)
		sprintf(year, "%04d", t->tm_year + 1900);
	else
		sprintf(year, "%02d", t->tm_year - 100);

	mday_pos = 3 * char_location('D', delim, c->date_format);
	mon_pos  = 3 * char_location('M', delim, c->date_format);
	year_pos = 3 * char_location('Y', delim, c->date_format);
	yearlen  = 2 + c->four_digit_year * 2;

	strncpy(date + mday_pos, mday, 2);
	strncpy(date + mon_pos,  mon,  2);
	strncpy(date + year_pos, year, yearlen);

	return date;
}

/* print a string, wrapping lines at word breaks */
void
print_long(FILE *f, const char *s, size_t maxlen)
{
	for (size_t len = strlen(s); len > 0; ) {
		if (len <= maxlen) {
			fwrite(s, 1, len, f);
			if (s[len - 1] != '\n') fputc('\n', f);
			break;
		}

		size_t end, next;
		for (end = maxlen; ; end--) {
			if (end == 0) {
				end = maxlen;
				next = maxlen;
				break;
			}

			if (s[end] == ' ') {
				next = end + 1;
				break;
			}
		}

		fwrite(s, 1, end, f);
		fputc('\n', f);
		s += next;
		len -= next;
	}
}

bool
is_alphanumeric(char c)
{
	return  ((is_ascii_int(c)     ||
		 (64 < c && c < 91 )) || /* lowercase */
		((96 < c && c < 122)));  /* uppercase */
}

/* returns true if c is 0-9 */
bool
is_ascii_int(char c)
{
	return (47 < c && c < 58);
}

