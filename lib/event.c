#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "conf.h"
#include "util.h"
#include "event.h"

static int eventtree_update_from_string(
    EventTree *et,
    char *path_l,
    const struct config *conf
);

struct limit
limit_new(const struct config *c)
{
    struct limit l;
    l.min = l.max = NULL;

    if (!c->wiki) {
        l.min = malloc(sizeof(struct tm));
        l.max = malloc(sizeof(struct tm));
        memcpy(l.min, c->now, sizeof(struct tm));
        memcpy(l.max, c->now, sizeof(struct tm));
		l.min->tm_mday -= c->days_before;
		l.max->tm_mday += c->days_after;
    }

	l.title_tar = c->target;
	l.categ_tar = c->tarcat;

    return l;
}

/* verify that an event is within the limit, if a field of limit is NULL
 * ignore. */
bool
matches_lim(const struct limit *l, const Event *e)
{
	if (l == NULL) return true;
	if (e == NULL) return false;

	if ((l->title_tar != NULL && e->title != NULL) &&
	     strcmp(e->title, l->title_tar) != 0)
		return false;
	if ((l->categ_tar != NULL && e->cat != NULL) &&
	     strcmp(e->cat, l->categ_tar) != 0)
		return false;
	if ((l->min != NULL && e->time != NULL) &&
		difftime(mktime(l->min), mktime(e->time)) > 0)
		return false;
	if ((l->max != NULL && e->time != NULL) &&
		difftime(mktime(l->max), mktime(e->time)) < 0)
		return false;

	return true;
}

/* read string in form `!KEY VALUE' to dynamically allocated KeyValue
 * struct.  If there is no key specified, it is assumed to be MISC.
 */
struct KeyValue *
key_value_read(const char *line)
{
        struct KeyValue *kv;
        char *key, *val;
        ssize_t key_len, val_len;

        if (line[0] == '!') {
                key = malloc(strlen(line) + 1);
                sscanf(line, "!%s ", key);
                /* + 1 for null terminator, + 1 for trailing space */
                key_len = strlen(key) + 2;
                val_len = strlen(line) - strlen(key);
                val = malloc(val_len);
                strcpy(val, line + key_len);
        } else {
                key = strdup("MISC");
                val = strdup(line);
        }

        kv = malloc(sizeof(struct KeyValue));
        kv->key = key;
        kv->val = val;
        return kv;
}

/* initialize a new empty Event on 0/0/1900 year 0:0:0 */
Event *
event_new_empty(void)
{
	Event *e = malloc(sizeof(Event));
	e->title        = NULL;
	e->cat          = NULL;
	e->description  = NULL;
	e->misc         = NULL;
	e->misc_cap     = 0;
	e->p            = 0;
	e->time         = tm_empty();
	e->short_disp   = false;

	return e;
}

/* initialize event with title TODAY on current date and time */
Event *
event_now(const struct config *conf)
{
	time_t t = time(NULL);
	struct tm *time = localtime(&t);

	Event *e = event_new_empty();
	e->title = malloc(15);
	*(e->time) = *time;
	e->short_disp  = true;

	sprintf(e->title, "\033[%smTODAY%s", conf->today_color,
	                                     RESET_COLOR);

	return e;
}

/* free up all the fields, assumes dynamically allocated or null */
Event *
event_free(Event *e)
{
	if (e == NULL) return NULL;
	if (e->title != NULL)       free(e->title);
	if (e->cat   != NULL)       free(e->cat);
	if (e->description != NULL) free(e->description);
	if (e->time != NULL)        free(e->time);
	if (e->misc != NULL)        free(e->misc);
	free(e);
	return NULL;
}

/* loop through file line-by-line to fill fields of event */
void
event_fill_from_text(Event *e, FILE *f, const struct config *c)
{
	struct KeyValue *pair;
	char   *file_line    = NULL;
	size_t  file_bufsize = 0;
	ssize_t len;

	while ((len = getline(&file_line, &file_bufsize, f)) > 0) {
		file_line[--len] = '\0'; /* strip newline */
		if (len < 2) continue;   /* ignore empty lines */

		pair = key_value_read(file_line);
		event_insert(e, pair, c);
		free(pair->val);
		free(pair->key);
		free(pair);
	}
	free(file_line);
}

/* write the event to stdout, decide on type of formatting */
void
event_disp(const Event *e, const struct config *c)
{
	if (e->short_disp || c->wiki) event_sdisp(e, c);
	else event_ndisp(e, c);
}

/* write the event to stdout with some pretty formatting ✨ */
void
event_ndisp(const Event *e, const struct config *c)
{
	char *time, *date;

	time = tm_tascii(e->time);
	date = tm_dascii(e->time, c);

	if (e->cat != NULL)
		printf("\033[38;5;%dm", buftocol(e->cat));

	for (unsigned int i = 0; i < c->col_width; i++) printf("-");

	/* bold title */
	printf("\n\033[1m");
	printf("%-*sTITLE\n", c->col_width - 5, e->title);
	printf(RESET_COLOR);
	if (e->cat != NULL)
		printf("\033[38;5;%dm", buftocol(e->cat));

	printf("%-*sDESCRIPTION\n", c->col_width - 11, e->description);
	printf("%-*sCATEGORY\n", c->col_width - 8, e->cat);

	printf("\033[%sm", c->pcolors[e->p]);

	printf("%s, (%d)\n", time, e->p);

	printf("%s\n", date);

	free(date);
	free(time);
	printf(RESET_COLOR);
}

/* display an event, but only on two lines (i.e., TODAY event) */
void
event_sdisp(const Event *e, const struct config *c)
{
	if (e->cat != NULL)
		printf("\033[38;5;%dm", buftocol(e->cat));
	for (unsigned int i = 0; i < c->col_width; i++) printf("-");

	if (e->cat != NULL)
		printf("\n%-*.*s%s", (int) (c->col_width - strlen(e->cat)),
				     (int) (c->col_width - strlen(e->cat)),
				     e->title, e->cat);
	else
		printf("\n%s", e->title);

	printf("%s\n", RESET_COLOR);
}

/* verbose display, display with miscellaneous field */
void
event_vdisp(const Event *e, const struct config *c) {
	event_disp(e, c);
	if (e->misc == NULL) return;
	print_long(stdout, e->misc, c->col_width);
}

/* parse a KeyValue into Event fields */
void
event_insert(Event* e, struct KeyValue *k, const struct config *conf)
{
	if (strcmp(k->key, "TITLE") == 0) {
		e->title = malloc((strlen(k->val) + 1) * sizeof(char));
		strcpy(e->title, k->val);
	} else if (strcmp(k->key, "DESCRIPTION") == 0) {
		e->description = malloc((strlen(k->val) + 1) * sizeof(char));
		strcpy(e->description, k->val);
	} else if (strcmp(k->key, "TIME") == 0) {
		e->time->tm_hour = match_int('H', k->val, conf->time_format);
		e->time->tm_min  = match_int('M', k->val, conf->time_format);
	} else if (strcmp(k->key, "DATE") == 0) {
		event_insert_date(e, k->val, conf);
	} else if (strcmp(k->key, "PRIORITY") == 0) {
		event_insert_priority(e, k->val);
	} else if (strcmp(k->key, "MISC") == 0) {
		event_insert_misc(e, k->val);
	}
}

/* insert provided date string into e->date */
void
event_insert_date(Event *e, const char *date, const struct config *conf)
{
	int day   = match_int('D', date, conf->date_format);
	int month = match_int('M', date, conf->date_format);
	int year  = match_int('Y', date, conf->date_format);

	if (year < 2000) year += 2000;
	year -= 1900;

	if (day == 0)
		day = e->time->tm_mday;
	if (month == 0)
		month = e->time->tm_mon;
	if (year == 0)
		year = e->time->tm_year;

	e->time->tm_mday   = day;
	e->time->tm_mon    = --month;
	e->time->tm_year   = year;
	e->time->tm_isdst = conf->now->tm_isdst;
}

/* insert category, free buffer if not NULL */
void
event_insert_category(Event *e, const char *p)
{
	if (e->cat == NULL) free(e->cat);
	e->cat = strdup(p);
}

/* insert a priority based on the PRIORITY_STR_* macros in global.h */
void
event_insert_priority(Event *e, char *text)
{
	if (strcmp(text, PRIORITY_STR_HIGH) == 0)
		e->p = 2;
	else if (strcmp(text, PRIORITY_STR_MID) == 0)
		e->p = 1;
	else if (strcmp(text, PRIORITY_STR_LOW) == 0)
		e->p = 0;
}

/* insert text into e->misc - set if empty, modify &(e->misc) otherwise */
void
event_insert_misc(Event *e, char *text)
{
    if (e == NULL || text == NULL) return;

	char **addr;
	addr = &(e->misc);

	if (text[0] == '\0') return;
	if (e->misc == NULL) {
		e->misc_cap = strlen(text) + 2;
		e->misc = malloc(e->misc_cap);
		strcpy(e->misc, text);
	} else buffer_append_str(addr, text, &(e->misc_cap));

	buffer_append(addr, '\n', &(e->misc_cap));
}

/* returns 0 if equal, below if a < b and 1 if b > a */
int
event_compare_time(const Event *a, const Event *b)
{
	time_t at = mktime(a->time);
	time_t bt = mktime(b->time);

	return (int) difftime(at, bt);
}

/* compare the events alphabetically, by category and then by name
 * case-insensitive */
int
event_compare_alpha(const Event *a, const Event *b)
{
	int cat = strcasecmp(a->cat, b->cat);
	if (cat == 0)
		return strcasecmp(a->title, b->title);
	else
		return cat;
}

/* return new empty event tree */
EventTree *
eventtree_new(void)
{
	EventTree *et = malloc(sizeof(EventTree));
	et->event = NULL;
	et->left  = NULL;
	et->right = NULL;
	/* by default, compare by time */
	et->cmp = &event_compare_time;
	return et;
}

EventTree *
eventtree_from(const struct config *c)
{
    EventTree *et = eventtree_new();
    if (c->wiki) et->cmp = &event_compare_alpha;
    eventtree_fromf(et, c, stdin);
    if (!c->wiki) et = eventtree_insert(et, event_now(c));

    return et;
}

/* build tree from file descriptor */
EventTree *
eventtree_fromf(EventTree *et, const struct config *conf, FILE *from)
{
	char *path_l;
	ssize_t len;
	size_t bufsize;

	path_l = NULL;
	bufsize = 0;
	while ((len = getline(&path_l, &bufsize, from)) > 0) {
	    eventtree_update_from_string(et, path_l, conf);
	}

	free(path_l);
	return et;
}

/* initialize tree node */
EventTree *
eventtree_new_from_event(Event *e)
{
	EventTree *et = eventtree_new();
	et->event = e;
	return et;
}

/* insert an event into provided node */
EventTree *
eventtree_insert(EventTree *et, Event *e)
{
	/* two potential instances of base case, either tree itself is null
	 * or the tree is functionally NULL, i.e., empty */
	if (et == NULL) {
		return eventtree_new_from_event(e);
	} else if (et->event == NULL) {
		et->event = e;
		return et;
	}

	int cmp = et->cmp(et->event, e);
	if (cmp < 0) {
		et->left  = eventtree_insert(et->left, e);
		et->left->cmp = et->cmp;
	} else {
		et->right = eventtree_insert(et->right, e);
		et->right->cmp = et->cmp;
	}

	return et;
}

/* recursively free a tree given root */
void *
eventtree_free(EventTree *et)
{
	if (et == NULL) return NULL;
	et->left  = eventtree_free(et->left);
	et->right = eventtree_free(et->right);
	et->event = event_free(et->event);
	free(et);
	return NULL;
}

/* perform an in-order traversal of the provided tree and
 * call the specified function on the values.
 */
void
eventtree_in_order(
    const EventTree *et,
    const struct config *c,
    const struct limit *l,
    void (*fun)(const Event *, const struct config *))
{
	if (et == NULL) return;
	eventtree_in_order(et->right, c, l, fun);
	if (matches_lim(l, et->event))
		fun(et->event, c);
	eventtree_in_order(et->left, c, l, fun);
}

/* perform function act on an event in the tree IFF cmp(et->event, c) returns
 * 0. Used to compare an event with config options.
 */
void
eventtree_if(const EventTree *et, const struct config *c,
             int (*cmp)(const Event *, const struct config *),
             void (*act)(const Event *, const struct config *))
{
	if (et == NULL) return;
	eventtree_if(et->left, c, cmp, act);
	if (cmp(et->event, c) == 0) act(et->event, c);
	eventtree_if(et->right, c, cmp, act);
}

int
cat_cmp(const Event *e, const struct config *c)
{
	if (e->cat == NULL || c->tarcat == NULL) return -1;
	return strcmp(e->cat, c->tarcat);
}

int
tar_cmp(const Event *e, const struct config *c)
{
	return strcmp(e->title, c->target);
}

/* Given a path to an event, update an event tree such that it contains the
 * content of the file pointed to by the path.
 * RETURNS:
 *  * -1 if the file does not exist.
 *  * -2 if path_l or configuration are NULL
 *  * 0 on succes
*/
static int
eventtree_update_from_string(
    EventTree *et,
    char *path_l,
    const struct config *conf
)
{
        if (path_l == NULL || conf == NULL) return -2;
        FILE *event_file;
        char *cat_l;
        int len = strlen(path_l);
		if (path_l[--len] == '\n') path_l[len] = '\0';
		cat_l = parent_dir(path_l);

		event_file = fopen(path_l, "r");
		if (event_file == NULL) return -1;

		Event *e = event_new_empty();
		event_insert_category(e, cat_l);
		event_fill_from_text(e, event_file, conf);

		et = eventtree_insert(et, e);
		fclose(event_file);
		free(cat_l);

		return 0;
}
