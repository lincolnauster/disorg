#define CONF_ENABLED(a) (a != NULL && strcmp(a, "TRUE") == 0)
/* holds global config options, by default read from environment variables in
 * main.c
 */
struct config {
    /* file hierarchy options */
    const char *base_directory;

	/* time strings and formatting */
	const char *date_format;
	const char *time_format;
	bool four_digit_year;

	/* display settings */
	const char *today_color;
	const char *pcolors[3];
	unsigned int col_width;

	/* options for building limit */
	int days_before;
	int days_after;
	char *target;
	char *tarcat;

	bool wiki;

	struct tm *now;
};

struct config conf_from_env();

unsigned int match_int(char, const char *, const char *);
unsigned int char_location(char, char, const char *);
char first_nonalpha(const char *);
unsigned int get_nth(int, char, const char *);

int conf_atoi(const char *);
int conf_strlen(const char *);
