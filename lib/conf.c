#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "conf.h"
#include "util.h"

struct config
conf_from_env()
{
    struct config conf;

    conf.base_directory = getenv("BASE_DIRECTORY");

    conf.date_format = getenv("DATE_FORMAT");
    conf.time_format = getenv("TIME_FORMAT");
    conf.four_digit_year = CONF_ENABLED(getenv("FOUR_DIGIT_YEAR"));

    conf.today_color = getenv("TODAY_COLOR");
    conf.pcolors[0] = getenv("PLOW_COLOR");
    conf.pcolors[1] = getenv("PMED_COLOR");
    conf.pcolors[2] = getenv("PHIGH_COLOR");

    if (!getenv("COL_WIDTH"))
        conf.col_width = 80;
    else
        conf.col_width = conf_atoi(getenv("COL_WIDTH"));

    conf.days_before = conf_atoi(getenv("DAYS_BEFORE"));
    conf.days_after  = conf_atoi(getenv("DAYS_AFTER"));

    conf.target = NULL;
    conf.tarcat = NULL;

    conf.wiki = CONF_ENABLED(getenv("WIKI"));

    time_t t = time(NULL);
    conf.now = localtime(&t);

    return conf;
}

/* given a format string (such as H:M:S) that corresponds to user data
 * (for instance 3:30:00), this returns the integer indicated by the
 * marker character. Returns 0 if not matched or if the delimeters of the
 * format string and that of the passed string do not match.
 */
unsigned int
match_int(char location_char, const char *string, const char *format_string)
{
	int location;
	char delim;
	delim = first_nonalpha(format_string);
	if (delim != first_nonalpha(string)) return 0;

	location = char_location(location_char, delim, format_string);

	return get_nth(location, delim, string);
}

/* get location of a character in a format string, relative to delimiter */
unsigned int
char_location(char location_char, char delim, const char *format_string)
{
	int location, len;
	char c;

	location = 0;
	len = strlen(format_string);

	for (int i = 0; i < len; i++) {
		c = format_string[i];
		if (c == delim)
			location++;
		else if (c == location_char)
			return location;
	}

	return 0;
}

/* returns the first non-alphanumeric char */
char
first_nonalpha(const char *s)
{
	char c;
	size_t len = strlen(s);
	for (size_t i = 0; i < len; i++) {
		c = s[i];
		if (!is_alphanumeric(c))
			return c;
	}
	return '\0';
}

unsigned int
get_nth(int location, const char delim, const char *string)
{
	char *copy, *copy_a, *token;
	char delim_str[2];

	token = NULL;
	// delim_str = malloc(2 * sizeof(char));
	delim_str[0] = delim;
	delim_str[1] = '\0';

	copy_a = strdup(string);
	copy   = copy_a;

	for (int i = 0; i < location + 1; i++) {
		token = strsep(&copy, delim_str);
		if (token == NULL) {
			free(copy_a);
			return 0;
		}
	}


	int ans = atoi(token);
	free(copy_a);
	return ans;
}

int
conf_atoi(const char *s)
{
	if (s == NULL) return 0;
	else return atoi(s);
}

int
conf_strlen(const char *s)
{
	if (s == NULL) return 0;
	else return strlen(s);
}
