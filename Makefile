all:
	gcc src/cli.c lib/conf.c lib/event.c lib/util.c \
		-o disorg-main -pedantic -Wall -Wextra -g
gui:
	valac src/gui.vala --pkg gtk4 --pkg gio-2.0 -o disorg.gtk
test:
	gcc lib/util.c lib/conf.c lib/event.c tests/tests.c tests/util-tests.c tests/event-tests.c \
	tests/conf-tests.c \
		-o tests/test -g
release:
	gcc src/cli.c lib/conf.c lib/event.c lib/util.c \
		-o disorg-main
install: release
	cp disorg-main disorg.sh /usr/local/bin/
uninstall:
	rm /usr/local/bin/disorg-main
	rm /usr/local/bin/disorg.sh
