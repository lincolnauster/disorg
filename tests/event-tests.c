#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "../lib/conf.h"
#include "../lib/util.h"
#include "../lib/event.h"

#include "tests.h"
#include "event-tests.h"

struct result
matches_lim_test(void)
{
	int total, passed;
	struct result res;
	total = passed = 0;
	int count = LEN(matches_lim_tests);

	for (int i = 0; i < count; i++) {
		struct MatchesLimTest test = matches_lim_tests[i];
		bool result = matches_lim(&test.l, &test.e);

		if (result = test.r)
			passed++;
		else
			printf(FAILED, "", "");

		total++;
	}

	res.passed = passed;
	res.total = total;
	return res;
}
