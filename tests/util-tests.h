struct KeyValueReadTest {
	char *s;
	struct KeyValue kv;
};

struct BufferAppendTest {
	char *a; char b; size_t s;
	char *r;
};

struct BufferAppendStrTest {
	char *a; char *b; size_t s;
	char *r;
};

struct ParentDirTest {
	char *a;
	char *p;
};

struct TmTasciiTest {
	struct tm tm;
	char *r;
};

struct TmDasciiTest {
	struct tm tm; struct config c;
	char *r;
};

struct PrintLongTest {
	const char *s; size_t n;
	char *r;
};

/* config structs for testing */
static const struct config CONFIG_TESTS[] = {
	{
    	"~/.disorg",
    	"D/M/Y", "H:M", true,
    	"91", { "31", "35", "0" }, 80,
    	0, 0, NULL, NULL,
    	false
	},

	{
	    "~/.disorg",
    	"D-M-Y", "H:M", true,
    	"91", { "31", "35", "0" }, 80,
    	0, 0, NULL, NULL,
    	false
	},
};

/* test cases are defined below, as constant arrays of the corresponding
 * structures. */

static const struct KeyValueReadTest key_value_read_tests[] = {
	{ "!TITLE this is a test",   { "TITLE", "this is a test"   } },
	{ "!lorem ipsum dolor\\sit", { "lorem", "ipsum dolor\\sit" } },
	{ "ABC ABC",                 { "MISC",   "ABC ABC"         } },
};

static const struct BufferAppendTest buffer_append_tests[] = {
	{ "abc",  'd',  4, "abcd" },
	{ "thi ", '\0', 4, "thi " }
};

static const struct BufferAppendStrTest buffer_append_str_tests[] = {
	{ "abc",   "def",     4, "abcdef"   },
	{ " thi",  "s is",    5, " this is" },
	{ "\0abc", "garbage", 4, "garbage"  },
};

static const struct ParentDirTest parent_dir_tests[] = {
	{ "/home/lincoln/Documents/disorg/tests/test.h", "tests"         },
	{ "/mnt/nas/test.png",                           "nas"           },
	{ "Documents/test.h",                            "Documents"     },
	{ "../Documents/test.h",                         "Documents"     },
	{ "..\\Documents/test.h",                        "..\\Documents" },
};

static const struct TmTasciiTest tm_tascii_tests[] = {
	{ { 0, 30, 12, 1, 0, 100, false }, "12:30" },
	{ { 1, 35, 0,  1, 0, 100, false }, "00:35" }
};

static const struct TmDasciiTest tm_dascii_tests[] = {
	{ { 0, 30, 12, 1, 0, 100, false },  CONFIG_TESTS[0], "01/01/2000" },
	{ { 1, 35, 0,  29, 0, 121, false }, CONFIG_TESTS[0], "29/01/2021" },
	{ { 0, 30, 12, 1, 0, 100, false },  CONFIG_TESTS[1], "01-01-2000" },
	{ { 1, 35, 0,  29, 0, 121, false }, CONFIG_TESTS[1], "29-01-2021" }
};

static const struct PrintLongTest print_long_tests[] = {
	{ "lorem ipsum.\n", 80, "lorem ipsum.\n"},
	{ "lorem ipsum.",   80, "lorem ipsum.\n"},
	{ "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi erat "
	  "orci, malesuada quis pharetra ac, gravida in elit. Fusce varius quis "
	  "mauris nec gravida. Vestibulum sapien lorem, hendrerit vitae cursus "
	  "vitae, lobortis finibus est. Donec mi metus, facilisis vitae lacinia "
	  "egestas, bibendum et risus.  Vestibulum maximus justo vitae ligula "
	  "feugiat hendrerit. Phasellus vulputate congue augue at iaculis. "
	  "Vivamus posuere augue quis mauris malesuada tincidunt.",
	  80,
"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi erat orci,\n"
"malesuada quis pharetra ac, gravida in elit. Fusce varius quis mauris nec\n"
"gravida. Vestibulum sapien lorem, hendrerit vitae cursus vitae, lobortis finibus\n"
"est. Donec mi metus, facilisis vitae lacinia egestas, bibendum et risus. \n"
"Vestibulum maximus justo vitae ligula feugiat hendrerit. Phasellus vulputate\n"
"congue augue at iaculis. Vivamus posuere augue quis mauris malesuada tincidunt.\n", },
    { "", 80, ""},

};

struct result key_value_read_test(void);
struct result buffer_append_test(void);
struct result buffer_append_str_test(void);
struct result parent_dir_test(void);
struct result tm_tascii_test(void);
struct result tm_dascii_test(void);
struct result print_long_test(void);
