#define LEN(a) sizeof(a) / sizeof(a[0])

#define FAILED "FAILED:\nexpected\n%s\ngot\n%s\n"

#define GREEN "\033[1;32m"
#define RESET "\033[0;m"

struct result {
	int passed;
	int total;
};
