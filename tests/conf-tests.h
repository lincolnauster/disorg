struct MatchIntTest {
	char t; char *s; char *f;
	unsigned int r;
};

struct CharLocationTest {
	char t; char d; char *s;
	unsigned int r;
};

struct FirstNonalphaTest {
	char *s;
	char r;
};

static const struct MatchIntTest match_int_tests[] = {
	{ 'A', "3.5.12",  "A.B.C", 3 },
	{ 'A', "03.5.12", "A.B.C", 3 },
	{ 'D', "03/5/12", "D/M/Y", 3 },
	{ 'D', "03/5/12", "D-M-Y", 0 },
	{ 'Y', "03/5/12", "D/M/Y", 12 },
};

static const struct CharLocationTest char_location_tests[] = {
	{ 'A', '-', "A-B-C", 0 },
	{ 'B', '-', "A-B-C", 1 },
	{ 'C', '-', "A-B-C", 2 },
	{ 'A', '/', "A-B-C", 0 },
	{ 'B', '/', "A-B-C", 0 },
};

static const struct FirstNonalphaTest first_nonalpha_tests[] = {
	{ "ABC-",    '-' },
	{ "A-BEOU*", '-'},
	{ "ABEO*U-", '*'},
};

struct result match_int_test(void);
struct result char_location_test(void);
struct result first_nonalpha_test(void);
