#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "../lib/conf.h"
#include "../lib/util.h"
#include "../lib/event.h"

#include "tests.h"
#include "util-tests.h"

struct result
key_value_read_test(void)
{
	int total, passed;
	struct result res;
	total = passed = 0;
	int count = LEN(key_value_read_tests);
	for (int i = 0; i < count; i++) {
		struct KeyValueReadTest test = key_value_read_tests[i];
		struct KeyValue *result = key_value_read(test.s);

		if (KEY_VALUE_CMP((*result), test.kv) == 0)
			passed++;
		else
			printf(FAILED, test.s, result->key);

		total++;
		free(result);
	}

	res.passed = passed;
	res.total = total;
	return res;
}

struct result
buffer_append_test(void)
{
	int total, passed;
	struct result res;
	total = passed = 0;
	int count = LEN(buffer_append_tests);
	for (int i = 0; i < count; i++) {
		struct BufferAppendTest test = buffer_append_tests[i];
		char *s = strdup(test.a);
		buffer_append(&s, test.b, &test.s);

		if (strcmp(s, test.r) == 0)
			passed++;
		else
			printf(FAILED, test.r, s);

		total++;
		free(s);
	}
	res.passed = passed;
	res.total = total;
	return res;
}

struct result
buffer_append_str_test(void)
{
	int total, passed;
	struct result res;
	total = passed = 0;
	int count = LEN(buffer_append_str_tests);
	for (int i = 0; i < count; i++) {
		struct BufferAppendStrTest test
		     = buffer_append_str_tests[i];
		char *a = strdup(test.a);
		buffer_append_str(&a, test.b, &test.s);

		if (strcmp(a, test.r) == 0)
			passed++;
		else
			printf(FAILED, test.r, a);

		total++;
		free(a);
	}

	res.passed = passed;
	res.total = total;
	return res;
}

struct result
parent_dir_test(void)
{
	int total, passed;
	struct result res;
	total = passed = 0;

	int count = LEN(parent_dir_tests);
	for (int i = 0; i < count; i++) {
		struct ParentDirTest test = parent_dir_tests[i];
		char *p = parent_dir(test.a);
		if (strcmp(p, test.p) == 0)
			passed++;
		else
			printf(FAILED, test.p, p);

		total++;
		free(p);
	}

	res.total = total;
	res.passed = passed;
	return res;
}

struct result
tm_tascii_test(void)
{
	int total, passed;
	struct result res;
	total = passed = 0;
	int count = LEN(tm_tascii_tests);
	for (int i = 0; i < count; i++) {
		struct TmTasciiTest test = tm_tascii_tests[i];
		char *r = tm_tascii(&(test.tm));
		if (strcmp(r, test.r) == 0)
			passed++;
		else
			printf(FAILED, test.r, r);

		total++;
		free(r);
	}

	res.total = total;
	res.passed = passed;
	return res;
}

struct result
tm_dascii_test(void)
{
	int total, passed;
	struct result res;
	total = passed = 0;
	int count = LEN(tm_dascii_tests);
	for (int i = 0; i < count; i++) {
		struct TmDasciiTest test = tm_dascii_tests[i];
		char *r = tm_dascii(&(test.tm), &(test.c));
		if (strcmp(r, test.r) == 0)
			passed++;
		else
			printf(FAILED, test.r, r);

		total++;
		free(r);
	}

	res.passed = passed;
	res.total = total;
	return res;
}

struct result
print_long_test(void)
{
	int total, passed;
	struct result res;
	total = passed = 0;

	char *r;
	size_t l;
	FILE *f;
	int count = LEN(print_long_tests);
	for (int i = 0; i < count; i++) {
		struct PrintLongTest test = print_long_tests[i];
		f = fopen("/tmp/disorg_test_print_long", "w+");

		if (f == NULL) {
			fprintf(stderr,
			        "couldn't open file, reporting as failed\n"
			        RESET);
			total++;
			return res;
		}

		/* make sure file is effectively empty */
		fseek(f, 0, SEEK_SET);
		fputc('\0', f);
		fseek(f, 0, SEEK_SET);

		/* run print_long */
		print_long(f, test.s, test.n);

		fclose(f);
		f = fopen("/tmp/disorg_test_print_long", "r");

		/* read the result of the file */
		fseek(f, 0, SEEK_END);
		l = ftell(f);
		fseek(f, 0, SEEK_SET);
		r = malloc(l);
		fread(r, 1, l, f);

		/* compare, aggregate */
		if (strcmp(r, test.r) == 0)
			passed++;
		else
			printf(FAILED, test.r, r);

		free(r);
		fclose(f);
		total++;
	}

	res.total = total;
	res.passed = passed;
	return res;
}
