#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#include "../lib/conf.h"

#include "tests.h"
#include "conf-tests.h"

struct result
match_int_test(void)
{
	int total, passed;
	struct result res;
	total = passed = 0;

	int count = LEN(match_int_tests);
	for (int i = 0; i < count; i++) {
		struct MatchIntTest test = match_int_tests[i];
		unsigned int r = match_int(test.t, test.s, test.f);

		if (r == test.r)
			passed++;
		else
			printf(FAILED, test.s, NULL);

		total++;
	}

	res.passed = passed;
	res.total = total;
	return res;
}

struct result
char_location_test(void)
{
	int total, passed;
	struct result res;
	total = passed = 0;

	int count = LEN(char_location_tests);
	for (int i = 0; i < count; i++) {
		struct CharLocationTest test = char_location_tests[i];
		unsigned int r = char_location(test.t, test.d, test.s);

		if (r == test.r)
			passed++;
		else
			printf(FAILED, test.s, NULL);

		total++;
	}

	res.passed = passed;
	res.total = total;
	return res;
}

struct result
first_nonalpha_test(void)
{
	int total, passed;
	struct result res;
	total = passed = 0;

	int count = LEN(first_nonalpha_tests);
	for (int i = 0; i < count; i++) {
		struct FirstNonalphaTest test = first_nonalpha_tests[i];
		char r = first_nonalpha(test.s);
		if (r == test.r)
			passed++;
		else
			printf(FAILED, test.s, NULL);

		total++;
	}

	res.passed = passed;
	res.total = total;
	return res;
}
