#include <stdbool.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>

#include "../lib/conf.h"
#include "../lib/event.h"
#include "../lib/util.h"

#include "tests.h"
#include "conf-tests.h"
#include "util-tests.h"
#include "event-tests.h"

static void
add_results(struct result *a, const struct result b)
{
	a->passed += b.passed;
	a->total += b.total;
}

int
main(void)
{
	struct result res;
	res.total = res.passed = 0;

	printf(GREEN "TESTING: util.h\n" RESET);
	printf(GREEN "         key_value_read\n" RESET );
	add_results(&res, key_value_read_test());

	printf(GREEN "         buffer_append\n" RESET);
	add_results(&res, buffer_append_test());

	printf(GREEN "         buffer_append_str\n" RESET);
	add_results(&res, buffer_append_str_test());

	printf(GREEN "         parent_dir\n" RESET);
	add_results(&res, parent_dir_test());

	printf(GREEN "         tm_tascii\n" RESET);
	add_results(&res, tm_tascii_test());

	printf(GREEN "         tm_dascii\n" RESET);
	add_results(&res, tm_dascii_test());

	printf(GREEN "         print_long\n" RESET);
	add_results(&res, print_long_test());

	printf(GREEN "TESTING: conf.h\n" RESET);
	printf(GREEN "         match_int\n" RESET );
	add_results(&res, match_int_test());

	printf(GREEN "         char_location\n" RESET );
	add_results(&res, char_location_test());

	printf(GREEN "         first_nonalpha\n" RESET );
	add_results(&res, first_nonalpha_test());

	printf(GREEN "TESTING: event.h\n" RESET);
	printf(GREEN "         matches_lim\n" RESET );
	add_results(&res, matches_lim_test());

	float percent = (float) res.passed / res.total * 100;
	printf("%d/%d %.2f%%\n", res.passed, res.total, percent);

	return res.total - res.passed;
}
